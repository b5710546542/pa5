import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
/**
 * UI that show the clock and have an alarm function.
 * @author Jidapar Jettananurak.
 *
 */
public class ClockUI {
	
	private Clock clock;
	private JPanel ground;
	private JPanel button;
	private JButton setButton;
	private JButton minusButton;
	private JButton plusButton;
	
	protected static JLabel hourLabel;
	protected static JLabel minuteLabel;
	protected static JLabel secondLabel;
	private JLabel saperate1;
	private JLabel saperate2;

	private File soundFile;
	protected static Clip clip;
	private AudioInputStream audioIn;
	
	private Date setalarm;
	protected static Date alarm;
	
	public ClockUI(){
		clock = new Clock();
		
		soundFile = new File("src/Sound/Loud-alarm-clock-sound.wav");
		try {
			audioIn = AudioSystem.getAudioInputStream(soundFile);
		} catch (UnsupportedAudioFileException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			clip = AudioSystem.getClip();
		} catch (LineUnavailableException e) {
			e.printStackTrace();
		}
		
		try {
			clip.open(audioIn);
		} catch (LineUnavailableException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		initComponents();
		
	}
	
	/**
	 * Create the clock and set function to button.
	 */
	public void initComponents(){
		JFrame frame = new JFrame();
		JPanel pane = new JPanel();
		pane.setLayout(new BoxLayout(pane , BoxLayout.Y_AXIS));
		
		ground = new JPanel();
		ground.setLayout(new FlowLayout());
		
		hourLabel = new JLabel();
		minuteLabel = new JLabel();
		secondLabel = new JLabel();
		saperate1 = new JLabel(":");
		saperate2 = new JLabel(":");
		
		clock.state.updateTime();
		
		ground.add(hourLabel);
		ground.add(saperate1);
		ground.add(minuteLabel);
		ground.add(saperate2);
		ground.add(secondLabel);
		
		button = new JPanel();
		button.setLayout(new FlowLayout());
		setButton = new JButton("SET");
		minusButton = new JButton("-");
		plusButton = new JButton("+");
		
		button.add(setButton);
		button.add(minusButton);
		button.add(plusButton);
		
		ActionListener task = new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				clock.state.updateTime();
				if(clock.equals(alarm)){
					clock.setState(clock.ALARM_STATE);
				}
			}
		};  
		javax.swing.Timer timer = new javax.swing.Timer(1000, task);
		
		ActionListener set = new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				if(clock.getState() == clock.DISPLAY_STATE){
					setalarm = new Date();
					clock.setState(clock.SET_HOUR);
				}
				else if(clock.getState() == clock.SET_HOUR){
					clock.setState(clock.SET_MINUTE);
				}
				else if(clock.getState() == clock.SET_MINUTE){
					clock.setState(clock.SET_SECOND);
				}
				else if(clock.getState() == clock.SET_SECOND){
					clock.setState(clock.DISPLAY_STATE);
					alarm = setalarm;
				}
				else if(clock.getState() == clock.ALARM_STATE){
					clock.setState(clock.DISPLAY_STATE);
					clip.stop();
				}
			}
		};
		setButton.addActionListener(set);
		
		ActionListener minus = new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				clock.state.updateTime(setalarm , -1);
			}
		};
		minusButton.addActionListener(minus);
		
		ActionListener plus = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(alarm != null && clock.state == clock.DISPLAY_STATE){
					clock.setState(clock.DISPLAY_ALARM);
				}else if(clock.state == clock.DISPLAY_ALARM){
					clock.setState(clock.DISPLAY_STATE);
				}
				clock.state.updateTime(setalarm , 1);
			}
		};
		plusButton.addActionListener(plus);
		
		timer.start();
		pane.add(ground);
		pane.add(button);
		
		frame.add(pane);
		frame.setVisible(true);
		frame.pack();
		frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
		
	}
	
}
