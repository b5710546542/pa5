import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.UnsupportedAudioFileException;

/**
 * Clock can show and set alarm time that have alarm sound. 
 * @author Jidapar Jettananurak.
 *
 */
public class Clock {

	protected ClockState state;

	private Calendar current;

	public Clock(){
		current = Calendar.getInstance();
		state = DISPLAY_STATE;
	}
	
	/**
	 * Show the current state.
	 * @return clockState
	 */
	public ClockState getState(){
		return state;
	}
	
	/**
	 * Set new state if new state is not old state, enter the new state.
	 * @param newState
	 */
	public void setState(ClockState newState){
		if(this.state != newState) newState.performSet();;

		this.state = newState;
	}

	/**
	 * Check between alarm time and current time if they are same time, return true.
	 * @param date is alarm time that user set.
	 * @return true if time are same.
	 */
	public boolean equals(Date date){
		if(date == null) return false;
		
		return this.current.get(Calendar.HOUR_OF_DAY) == date.getHours() && this.current.get(Calendar.MINUTE) == date.getMinutes() && this.current.get(Calendar.SECOND) == date.getSeconds();
	}
	
	/**
	 * State for show current time. 
	 */
	ClockState DISPLAY_STATE = new ClockState(){

		public void performSet() {
			ClockUI.hourLabel.setForeground(Color.black);
			ClockUI.minuteLabel.setForeground(Color.black);
			ClockUI.secondLabel.setForeground(Color.black);
			updateTime();
		}

		public void updateTime() {
			current = Calendar.getInstance();
			ClockUI.hourLabel.setText(String.format("%02d", current.get(Calendar.HOUR_OF_DAY)));
			ClockUI.minuteLabel.setText(String.format("%02d", current.get(Calendar.MINUTE)));
			ClockUI.secondLabel.setText(String.format("%02d", current.get(Calendar.SECOND)));
		}

		public void updateTime(Date alarm, int num) {
			
		}
	};
	
	/**
	 * State for show time that user set at last.
	 */
	ClockState DISPLAY_ALARM = new ClockState(){

		public void performSet() {
			ClockUI.hourLabel.setForeground(Color.blue);
			ClockUI.minuteLabel.setForeground(Color.blue);
			ClockUI.secondLabel.setForeground(Color.blue);
			updateTime();
		}

		public void updateTime() {
			ClockUI.hourLabel.setText(String.format("%02d", ClockUI.alarm.getHours()));
			ClockUI.minuteLabel.setText(String.format("%02d", ClockUI.alarm.getMinutes()));
			ClockUI.secondLabel.setText(String.format("%02d", ClockUI.alarm.getSeconds()));
		}

		public void updateTime(Date alarm, int num) {
			
		}
		
	};
	
	/**
	 * State for alarm it have warning sound. 
	 */
	ClockState ALARM_STATE = new ClockState(){

		public void performSet() {
			ClockUI.clip.start();
			ClockUI.clip.loop(Clip.LOOP_CONTINUOUSLY);
			ClockUI.hourLabel.setForeground(Color.red);
			ClockUI.minuteLabel.setForeground(Color.red);
			ClockUI.secondLabel.setForeground(Color.red);
		}

		public void updateTime() {
			current = Calendar.getInstance();
			DISPLAY_STATE.updateTime();
			if(current.get(Calendar.SECOND)%2 == 0){
				performSet();
			}
			else{
				ClockUI.hourLabel.setForeground(Color.black);
				ClockUI.minuteLabel.setForeground(Color.black);
				ClockUI.secondLabel.setForeground(Color.black);
			}
		}

		public void updateTime(Date alarm, int num) {
			
		}
	};

	/**
	 * State for set hour.
	 */
	ClockState SET_HOUR = new ClockState(){

		public void performSet() {
			ClockUI.hourLabel.setForeground(Color.red);
		}

		public void updateTime() {

		}
		
		public void updateTime(Date alarm , int num){
			alarm.setHours(alarm.getHours()+num);
			ClockUI.hourLabel.setText(String.format("%02d", alarm.getHours()));
		}
	};
	
	/**
	 * State for set minutes.
	 */
	ClockState SET_MINUTE = new ClockState(){

		public void performSet() {
			ClockUI.hourLabel.setForeground(Color.black);
			ClockUI.minuteLabel.setForeground(Color.red);
		}

		public void updateTime() {

		}

		public void updateTime(Date alarm, int num) {
			alarm.setMinutes(alarm.getMinutes()+num);
			ClockUI.minuteLabel.setText(String.format("%02d", alarm.getMinutes()));
		}
	};
	
	/**
	 * State for set second.
	 */
	ClockState SET_SECOND = new ClockState(){

		public void performSet() {
			ClockUI.minuteLabel.setForeground(Color.black);
			ClockUI.secondLabel.setForeground(Color.red);
		}

		public void updateTime() {

		}

		public void updateTime(Date alarm, int num) {
			alarm.setSeconds(alarm.getSeconds()+num);
			ClockUI.secondLabel.setText(String.format("%02d", alarm.getSeconds()));
		}
	};
	
}
