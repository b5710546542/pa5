import java.time.Clock;
import java.util.Date;
/**
 * State of clock use in show the clock function.
 * @author Jidapar Jettananurak.
 *
 */
public abstract class ClockState {

	/** handle "set" key press */
	public abstract void performSet( ) ;
	
	/** handle updateTime event notification */
	public abstract void updateTime();
	
	/**
	 * Set alarm time that use button. 
	 * @param alarm is the time that user can set time to alarm
	 * @param num is number that use for set alarm time
	 */
	public abstract void updateTime(Date alarm , int num);
	
	
}

